import React from "react";
import actionTypes from "../data/ActionTypes";

const MainReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.SET_TRANSACTIONS: {
      return { ...state, transactions: action.value };
    }

    case actionTypes.SET_INVESTMENTS: {
      return { ...state, investments: action.value };
    }

    case actionTypes.SET_CARD_BALANCE: {
        return { ...state, cardBalance: action.value }
    }

    case actionTypes.SET_CREDIT_BALANCE: {
        return { ...state, creditBalance: action.value }
    }

    case actionTypes.SET_INVESTMENTS_TOTAL: {
      return { ...state, investmentsTotal: action.value };
    }

    case actionTypes.SET_TRANSACTIONS_TOTAL: {
      return { ...state, transactionsTotal: action.value };
    }

    case actionTypes.SET_TOTAL_BALANCE: {
      return { ...state, totalBalance: action.value };
    }

    default:
      return state;
  }
};

export default MainReducer;
