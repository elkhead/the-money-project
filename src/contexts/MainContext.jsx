import React, { createContext, useReducer, useEffect } from "react";
import { initialState } from "../data/InitialState";
import MainReducer from "./MainReducer";
import { actions } from "../data/Actions";

export const MainContext = createContext(initialState);

export const MainProvider = ({ children }) => {
  const [state, dispatch] = useReducer(MainReducer, initialState);

  const stateVariables = {
    transactions : state.transactions,
    investments : state.investments,
    cardBalance: state.cardBalance,
    creditBalance: state.creditBalance,
    investmentsTotal : state.investmentsTotal,
    transactionsTotal: state.transactionsTotal,
    totalBalance: state.totalBalance
  }

  const actionFunctions = {
    setTransactions : (newVal) => dispatch(actions.setTransactions(newVal)),
    setInvestments : (newVal) => dispatch(actions.setInvestments(newVal)),
    setCardBalance : (newVal) => dispatch(actions.setCardBalance(newVal)),
    setCreditBalance : (newVal) => dispatch(actions.setCreditBalance(newVal)),
    setInvestmentsTotal : (newVal) => dispatch(actions.setInvestmentsTotal(newVal)),
    setTransactionsTotal : (newVal) => dispatch(actions.setTransactionsTotal(newVal)),
    setTotalBalance : (newVal) => dispatch(actions.setTotalBalance(newVal))
  }

  const store = {
    stateVariables,
    actionFunctions
  };

  return <MainContext.Provider value={store}>{children}</MainContext.Provider>;
};
