import React from "react";
import './EmptyCard.css';

const EmptyCard = ({ children }) => {
  return <div className="empty-card">
      { children }
  </div>;
};

export default EmptyCard;
