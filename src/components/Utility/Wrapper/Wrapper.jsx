import React from 'react';
import './Wrapper.css';

const Wrapper = ({ children }) => {
    return (
        <div className='simple-wrapper'>
            { children }
        </div>
    )
}

export default Wrapper
