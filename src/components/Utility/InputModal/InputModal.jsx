import { Grid } from '@mui/material';
import React, { useState, useEffect } from 'react'
import { getIsColumnsToDeleteEmpty, isColumnVisible } from '../../../helpers/objectFunctions';
import Wrapper from '../Wrapper/Wrapper'
import './InputModal.css';

const InputModal = ({ title, item, submit, closeModal, isEditItem, columnsToDelete }) => {
  const [isChecked, setIsChecked] = useState(false);
  const [inputValues, setInputValues] = useState({});
  const isColumnsToDeleteEmpty = getIsColumnsToDeleteEmpty(columnsToDelete);

  useEffect(() => {
    setInputValues(item);
  }, [item]);

  const handleSubmit = (e) => {
    e.preventDefault();
    submit(inputValues, isChecked);
    setInputValues(item);
  }

  const formInputs = () => {
    return Object.keys(inputValues).map((currentValue, index) => 
      isColumnVisible(currentValue, isColumnsToDeleteEmpty, columnsToDelete) && 
      <input
        key={index}
        className='input'
        type={ currentValue === 'date' ? 'date' : (currentValue === 'amount' ? 'number' : 'text')}
        required
        placeholder={currentValue.charAt(0).toUpperCase() + currentValue.slice(1)}
        value={inputValues[currentValue]}
        onChange={ (e) => setInputValues({ ...inputValues, [currentValue]: e.target.value }) }
        />
    );
  }

  const addCheckbox = () => {
    return (
      <Grid container item xs={12} paddingBottom={2} paddingTop={1}>
        <input
          type="checkbox"
          name="log-another-check"
          checked={isChecked ? "checked" : ""}
          onChange={() => setIsChecked(isChecked => !isChecked)}
        />
        <label
          className="log-another-label"
          onClick={() => setIsChecked(isChecked => !isChecked)}
        >
          Add another
        </label>
      </Grid>
    );
  };

  const submitAndCloseButtons = () => {
    return <Grid container spacing={1}>
      <Grid item xs={6}>
        <button
          className='btn submit-btn'
          type="submit"
          >Submit</button>
      </Grid>
      <Grid item xs={6}>
        <button
          className='btn close-btn'
          onClick={(e) => closeModal(e)}>Close</button>
      </Grid>
    </Grid>
  }

  return (
    <div className="modal-on">
      <form name='form' className='modal' onSubmit={(e) => handleSubmit(e)}>
        <Wrapper>
          <h3 className='title'>{ title }</h3>
          { formInputs() }
          { !isEditItem && addCheckbox() }
          { submitAndCloseButtons() }
        </Wrapper>
      </form>
    </div>
  )
}

export default InputModal
