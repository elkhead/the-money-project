import React from "react";
import ListItemsHeader from "./ListItemsHeader";
import ListItemsBody from "./ListItemsBody";
import "./ListItems.css";
import {
  getActionsCount,
  getIsActionsEmpty,
  getColumnsToDeleteCount,
  getColumnsWith,
  getHeader,
  getIsColumnsToDeleteEmpty
} from "../../../helpers/objectFunctions";

const ListItems = ({ objectsArray, showHeader, actions, columnsToDelete }) => {
  const isActionsEmpty = getIsActionsEmpty(actions);
  const actionsCount = isActionsEmpty ? 0 : getActionsCount(actions);
  const isColumnsToDeleteEmpty = getIsColumnsToDeleteEmpty(columnsToDelete);
  const columnsToDeleteCount = getColumnsToDeleteCount(columnsToDelete);
  const header = getHeader(objectsArray, columnsToDelete, isColumnsToDeleteEmpty);
  const columnsWidth = getColumnsWith(
    objectsArray,
    actionsCount,
    columnsToDeleteCount
  );
  
  const handleActions = (objectItem) => {
    const actionHandlers = Object.keys(actions).map((currentAction, index) => {
      const { title, action } = actions[currentAction];
      return (
        <button
          key={index}
          className="btn lic-btn"
          style={{ width: columnsWidth }}
          onClick={() => action(objectItem)}
        >
          {title}
        </button>
      );
    });

    return <span>{actionHandlers}</span>
  };

  return (
    <div>
      {showHeader && (
        <ListItemsHeader header={header} columnsWidth={columnsWidth} />
      )}
      <ListItemsBody
        objectsArray={objectsArray}
        columnsWidth={columnsWidth}
        isActionsEmpty={isActionsEmpty}
        handleActions={handleActions}
        columnsToDelete={columnsToDelete}
        isColumnsToDeleteEmpty={isColumnsToDeleteEmpty}
      />
    </div>
  );
};

export default ListItems;
