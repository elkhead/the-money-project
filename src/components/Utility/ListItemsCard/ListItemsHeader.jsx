import React from "react";

const ListItemsHeader = ({ header, columnsWidth }) => {
  return (
    <ul className="list-item-row header">
      {header.map((item, index) => {
        return (
          <li style={{ width: columnsWidth }} key={index}>
            {item.charAt(0).toUpperCase() + item.slice(1)}
          </li>
        );
      })}
      <hr />
    </ul>
  );
};

export default ListItemsHeader;