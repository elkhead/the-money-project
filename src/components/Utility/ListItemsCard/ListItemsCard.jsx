import { AddBoxOutlined } from "@mui/icons-material";
import React from "react";
import Wrapper from "../Wrapper/Wrapper";
import "./ListItemsCard.css";

const ListItemsCard = (props) => {
  const {
    title,
    items,
    width,
    addItem,
    editAndDeleteItem,
    addItemHandler,
    editItemHandler,
    deleteItemHandler,
    children
  } = props;

  const columns =
    editAndDeleteItem === true && editAndDeleteItem !== undefined
      ? (100 / (width + 1)).toString() + "%"
      : (100 / width).toString() + "%";

  const handleDelete = item => {
  };

  const formatListHeader = () => {
    return (title === "" || title === undefined) &&
      (addItem === false || addItem === undefined) ? (
      <></>
    ) : (
      <div className="list-items-card__header">
        {title !== undefined && title.length > 1 ? (
          <h3 className="lic-title">{title}</h3>
        ) : (
          <></>
        )}
        {addItem !== undefined && addItem === true ? (
          <AddBoxOutlined
            className="add-button"
            onClick={() => addItemHandler(title)}
            style={{ fontSize: "32px", color: "green" }}
          />
        ) : (
          <></>
        )}
      </div>
    );
  };

  const formatList = () => {
    return items.map((item, index) => {
      return (
        <ul key={index} className="list-item-row">
          {Object.keys(item).map((itemKey, itemIndex) => {
            return (
              <li
                style={{ width: columns }}
                key={itemIndex}
                className={"item-" + itemKey}
              >
                {item[itemKey]}
              </li>
            );
          })}
          <span>{children}</span>
          {editAndDeleteItem !== undefined && editAndDeleteItem === true ? (
            <span>
              <button
                className="btn lic-btn edit-item"
                onClick={() => editItemHandler(item, title)}
              >
                Edit
              </button>
              <button
                className="btn lic-btn delete-item"
                onClick={() => handleDelete(item, title)}
              >
                Delete
              </button>
            </span>
          ) : (
            <></>
          )}
          <hr />
        </ul>
      );
    });
  };

  return (
    <div className="list-items-card">
      <Wrapper>
        {formatListHeader()}
        {formatList()}
      </Wrapper>
    </div>
  );
};

export default ListItemsCard;
