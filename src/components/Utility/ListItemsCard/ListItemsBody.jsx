import React from "react";
import { getIsColumnsToDeleteEmpty, isColumnVisible } from "../../../helpers/objectFunctions";

const ListItemsBody = ({
  objectsArray,
  columnsWidth,
  isActionsEmpty,
  handleActions,
  columnsToDelete,
  isColumnsToDeleteEmpty
}) => {
  return objectsArray.map((item, index) => {
    return (
      <ul key={index} className="list-item-row">
        {Object.keys(item).map((itemKey, itemIndex) => {
          return (
            isColumnVisible(
              itemKey,
              isColumnsToDeleteEmpty,
              columnsToDelete
            ) && (
              <li style={{ width: columnsWidth }} key={itemIndex}>
                {item[itemKey]}
              </li>
            )
          );
        })}
        {!isActionsEmpty && handleActions(item)}
        <hr />
      </ul>
    );
  });
};

export default ListItemsBody;
