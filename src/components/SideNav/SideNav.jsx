import { 
  ReceiptLongOutlined, 
  CreditCard, 
  SwitchAccountOutlined, 
  SettingsApplicationsOutlined 
} from '@mui/icons-material';
import { Grid } from '@mui/material';
import React from 'react';
import './SideNav.css';

const SideNav = () => {
  return (
      <div className="main side-nav">
          <h3 className="nav-item side-nav-title">Hi Whitney</h3>
          <hr/>
          <Grid container item xs={1} className="side-nav-items">
              <Grid style={{ gap: 15 }} item className="nav-item side-nav-item">
                  <ReceiptLongOutlined sx={{ fontSize: 15 }}/>
                  <a>Transactions</a>
              </Grid>
              <Grid style={{ gap: 15 }} item className="nav-item side-nav-item">
                  <CreditCard sx={{ fontSize: 15 }}/>
                  <a>Cards</a>
              </Grid>

              <Grid style={{ gap: 15 }} item className="nav-item side-nav-item">
                  <SwitchAccountOutlined sx={{ fontSize: 15 }}/>
                  <a>Account</a>
              </Grid>
              <Grid style={{ gap: 15 }} item className="nav-item side-nav-item">
                  <SettingsApplicationsOutlined sx={{ fontSize: 15 }}/>
                  <a>Settings</a>
              </Grid>
          </Grid>
      </div>
  );
}

export default SideNav;