import React, { useContext, useEffect, useState } from "react";
import Wrapper from "../Utility/Wrapper/Wrapper";
import { MainContext } from "../../contexts/MainContext";
import "./Investments.css";
import EmptyCard from '../Utility/EmptyCard/EmptyCard';
import ListItems from '../Utility/ListItemsCard/ListItems';
import { AddBoxOutlined } from "@mui/icons-material";
import InputModal from "../Utility/InputModal/InputModal";
import { getEmptyObjectvalues } from "../../helpers/objectFunctions";

const InvestmentsCard = () => {
  const { stateVariables, actionFunctions } = useContext(MainContext);
  const { setInvestments, setInvestmentsTotal } = actionFunctions;
  const { investments } = stateVariables;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedInvestment, setSelectedInvestment] = useState(investments[0]);
  const [isEditInvestment, setIsEditInvestment] = useState(false);

  const title = 'Investments';
  const columnsToDelete = ['id'];
  const emptyInvestments = getEmptyObjectvalues(selectedInvestment);

  const addInvestment = investment => {
    setSelectedInvestment(emptyInvestments);
    setIsModalOpen(!isModalOpen);
    isEditInvestment && setIsEditInvestment(!isEditInvestment);
  }

  const editInvestment = investment => {
    setSelectedInvestment(investment);
    setIsModalOpen(!isModalOpen);
    !isEditInvestment && setIsEditInvestment(!isEditInvestment);
  }

  const sellInvestment = investment => {
    const x = investments.filter(
      (currentInvestment) => currentInvestment.id !== investment.id);
    
      console.log("x: ", x)
    setInvestments(
      x
    );
    console.log('selling');
  }

  const newInvestment = investment => {
    const investmentId = Number(investments[0].id) + 1;
    investment.id = investmentId;
    return investment;
  }

  const submitInvestment = (investment, isChecked) => {
    investment.amount = Number(investment.amount);
    if (isEditInvestment) {
      const investmentIndex = investments.findIndex(
        (obj) => obj.id === investment.id
      );
      investments[investmentIndex] = investment;
      setInvestments(investments);
    } else {
      setInvestments([newInvestment(investment), ...investments]);
    }
    !isChecked && setIsModalOpen(!isModalOpen);
  }

  const closeModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const investmentsActions = {
    editInvestment: {
      title: 'Edit',
      action: editInvestment,
    },

    deleteInvestment: {
      title: 'Sell',
      action: sellInvestment,
    }
  };

  const updateInvestmentsBalance = () => {
    setInvestmentsTotal(
      investments.reduce(
        (total, investment) => investment.amount + total, 0
      )
    );
  }

  useEffect(() => {
    updateInvestmentsBalance();
  }, [JSON.stringify(investments)]);

  return <Wrapper>
    {isModalOpen && (
        <InputModal
          title={title}
          item={selectedInvestment}
          submit={submitInvestment}
          closeModal={closeModal}
          isEditItem={isEditInvestment}
          columnsToDelete={columnsToDelete}
        />
      )}
      <EmptyCard>
        <div className="unpadded-top">
          <div className="investments-header">
            <h3>{ title }</h3>
            <div className="add-container">
              <AddBoxOutlined
                className="add-button"
                style={{ fontSize: "32px" }}
                onClick={() => addInvestment()}
              />
            </div>
          </div>
          <ListItems
          objectsArray={investments}
          showHeader={true}
          columnsToDelete={columnsToDelete}
          actions={investmentsActions}
          />
      </div>
    </EmptyCard>
  </Wrapper>
};

export default InvestmentsCard;
