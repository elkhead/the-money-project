import React, { useContext, useEffect } from "react";
import { MainContext } from "../../contexts/MainContext";
import EmptyCard from "../Utility/EmptyCard/EmptyCard";
import "./Balance.css";

const Balance = () => {
  const { stateVariables, actionFunctions } = useContext(MainContext);
  const {
    totalBalance,
    transactions,
    transactionsTotal,
    investmentsTotal,
    cardBalance,
    creditBalance,
  } = stateVariables;
  const { setTotalBalance, setTransactionsTotal } = actionFunctions;

  const calculateBalance = () => {
    const newTotalBalance =
      investmentsTotal + cardBalance + creditBalance - transactionsTotal;
    setTotalBalance(newTotalBalance);
  };

  useEffect(() => {
    calculateBalance();
  }, [transactionsTotal]);

  return (
    <EmptyCard>
      <p style={{ textAlign: "center", fontSize: "13px", color: "grey" }}>
        Total Balance
      </p>
      <h2 style={{ color: "green", textAlign: "center" }}>
        ${totalBalance.toLocaleString()}
      </h2>
    </EmptyCard>
  );
};

export default Balance;
