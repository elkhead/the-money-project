import React from "react";
import Wrapper from "../Utility/Wrapper/Wrapper";
import Transactions from "../Transactions/Transactions";
import Investments from "../Investments/Investments";
import { Grid } from "@mui/material";
import LineGraph from "../LineGraph/LineGraph";
import SectionHeader from "./SectionHeader";
import "./MainSection.css";

const MainSection = () => {
  return (
    <Wrapper>
      <div className="main-section">
        <SectionHeader />
        <Grid container rowSpacing={3} paddingBottom={4}>
          <Grid item xs={6}>
            <LineGraph />
          </Grid>
          <Grid item xs={6}>
            <LineGraph />
          </Grid>
          <Grid item xs={12}>
            <Transactions />
          </Grid>
          <Grid item xs={6}>
            <Investments />
          </Grid>
          <Grid item xs={6}>
            <Investments />
          </Grid>
        </Grid>
      </div>
    </Wrapper>
  );
};

export default MainSection;
