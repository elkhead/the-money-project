import { Grid } from "@mui/material";
import React, { useContext } from "react";
import { MainContext } from "../../contexts/MainContext";
import Balance from "../Balance/Balance";
import BalanceShelf from "../BalanceShelf/BalanceShelf";

const SectionHeader = () => {
  const { stateVariables } = useContext(MainContext);
  const { totalBalance } = stateVariables;
  
  return (
    <Grid container className="main-section-header">
      <Grid item xs={3}>
        <h3 className="transactions-title">Overview</h3>
        <p className="app-description">Personal finance manager</p>
        <h3>Balance</h3>
      </Grid>
      <Grid container item xs={9} columnSpacing={3} paddingTop="20px" alignItems="center">
        <Grid item xs={4}>
            <Balance />
        </Grid>
        <Grid container item xs={8}>
          <Grid item xs={4}>
            <button className="button button5">Deposit</button>
          </Grid>
          <Grid item>
            <button className="button button5">Withdraw</button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12}>
          <BalanceShelf />
      </Grid>
    </Grid>
  );
};

export default SectionHeader;
