import React from "react";
import EmptyCard from "../Utility/EmptyCard/EmptyCard";
import Wrapper from "../Utility/Wrapper/Wrapper";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'

const options = {
    title: { text: null },
    chart: {
        // type: 'area',
        height: 200,
        backgroundColor: '#141414',
        // width: 300,
        // width: (20 / 20 * 100) + '%'// 16:9 ratio
    },
    xAxis: {
        title: { text: 'title', style: { color: 'white' }},
        tickLength: 0,
        lineWidth: 0,
        labels: {
            style: {
                color: 'white'
            }
        }
    },
    yAxis: {
        title: { text: 'Balance', style: { color: 'white' }},
        gridLineWidth: 0,
        labels: {
            style: {
                color: 'white'
            }
        }
    },
    legend: {
        enabled: false
    },

    credits: {
        enabled: false
    },
    series: [{
        data: [10, 9, 5, 5, 4, 6, 12, 12, 13, 15, 18],
        marker: { enabled: false },
        lineColor: 'green',
        color: 'green'
    }]
}

const LineGraph = () => {
  return <Wrapper>
      <EmptyCard>
          <div className="padded-around">
            <HighchartsReact highcharts={Highcharts} options={options} className="zz"/>
          </div>
      </EmptyCard>
  </Wrapper>
};

export default LineGraph;
