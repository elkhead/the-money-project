import { Grid } from "@mui/material";
import React from "react";
import Investments from "../Investments/Investments";
import EmptyCard from "../Utility/EmptyCard/EmptyCard";
import Wrapper from "../Utility/Wrapper/Wrapper";

const Accounts = () => {
  return <Wrapper>
    <h3>Accounts</h3>
    <Grid container rowSpacing={2} paddingTop={4}>
      <Grid item xs={12}>
        <Investments />
      </Grid>
      <Grid item xs={12}>
        <Investments />
      </Grid>
    </Grid>
  </Wrapper>
};

export default Accounts;
