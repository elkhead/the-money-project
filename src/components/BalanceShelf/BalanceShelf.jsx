import { Grid } from '@mui/material';
import React, { useContext } from 'react';
import { MainContext } from '../../contexts/MainContext';
import EmptyCard from '../Utility/EmptyCard/EmptyCard';
import './BalanceShelf.css';

const BalanceShelf = () => {
    const { stateVariables } = useContext(MainContext);
    const { cardBalance, creditBalance, investmentsTotal } = stateVariables;

    return (
        <EmptyCard>
            <Grid container className='padded-around'>
                <Grid item xs={3}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <p className="bal total-balance">Card</p>
                            <p className="balance">${cardBalance.toLocaleString()}</p>
                        </Grid>
                        <Grid item xs={6}>
                            <p className="percentage" 
                                style={{ backgroundColor: "#70ffa767", color: "#51ff8b" }}>50%</p>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={1.5}>
                    <div className="bc-vl"/>
                </Grid>
                <Grid item xs={3}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <p className="bal card-balance">Credit</p>
                            <p className="balance">${creditBalance.toLocaleString()}</p>
                        </Grid>
                        <Grid item xs={6}>
                            <p className="percentage" 
                                style={{ backgroundColor: "#fd5d5d50", color: "#ff5d5d" }}>28%</p>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={1.5}>
                    <div className="bc-vl"/>
                </Grid>
                <Grid item xs={3}>
                    <Grid container spacing={3}>
                        <Grid item xs={6}>
                            <p className="bal credit-balance">Investments</p>
                            <p className="balance">${investmentsTotal.toLocaleString()}</p>
                        </Grid>
                        <Grid item xs={6}>
                            <p className="percentage" 
                                style={{ backgroundColor: "#70ffa767", color: "#51ff8b" }}>38%</p>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </EmptyCard>
    );
}

export default BalanceShelf;
