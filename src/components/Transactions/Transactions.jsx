import React, { useContext, useEffect, useState } from "react";
import { MainContext } from "../../contexts/MainContext";
import Wrapper from "../Utility/Wrapper/Wrapper";
import ListItems from "../Utility/ListItemsCard/ListItems";
import EmptyCard from "../Utility/EmptyCard/EmptyCard";
import InputModal from "../Utility/InputModal/InputModal";
import { AddBoxOutlined } from "@mui/icons-material";
import './Transactions.css';
import { getEmptyObjectvalues } from "../../helpers/objectFunctions";

const Transactions = () => {
  const { stateVariables, actionFunctions } = useContext(MainContext);
  const { transactions } = stateVariables;
  const { setTransactions, setTransactionsTotal } = actionFunctions;
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedTransaction, setSelectedTransaction] = useState(transactions[0]);
  const [isEditTransaction, setIsEditTransaction] = useState(false);

  const title = "Transactions";
  const columnsToDelete = 'id';
  const emptyTransactions = getEmptyObjectvalues(selectedTransaction)

  const editTransaction = (transaction) => {
    setSelectedTransaction(transaction);
    setIsModalOpen(!isModalOpen);
    !isEditTransaction && setIsEditTransaction(!isEditTransaction);
  };

  const addTransaction = () => {
    setSelectedTransaction(emptyTransactions);
    setIsModalOpen(!isModalOpen);
    isEditTransaction && setIsEditTransaction(!isEditTransaction);
  };

  const deleteTransaction = (transaction) => {
    setTransactions(
      transactions.filter(
        (currentTransaction) => currentTransaction.id !== transaction.id)
    );
  };

  const newTransaction = transaction => {
    const transactionId = Number(transactions[0].id) + 1;
    transaction.id = transactionId;
    return transaction;
  }

  const updateTransactionsBalance = () => {
    setTransactionsTotal(
      transactions.reduce(
        (total, transaction) => transaction.amount + total, 0
      )
    );
  }

  const submitTransaction = (transaction, isChecked) => {
    transaction.amount = Number(transaction.amount);
    if (isEditTransaction) {
      const transactionIndex = transactions.findIndex(
        (obj) => obj.id === transaction.id
      );
      transactions[transactionIndex] = transaction;
      setTransactions(transactions);
    } else {
      setTransactions([newTransaction(transaction), ...transactions]);
    }
    !isChecked && setIsModalOpen(!isModalOpen);
  };

  useEffect(() => {
    updateTransactionsBalance()
  }, [JSON.stringify(transactions)]);

  const closeModal = () => {
    setIsModalOpen(!isModalOpen);
  };

  const transactionsActions = {
    editTransaction: {
      title: "Edit",
      action: editTransaction,
    },

    deleteTransaction: {
      title: "Delete",
      action: deleteTransaction,
    },
  };

  return (
    <Wrapper>
      {isModalOpen && (
        <InputModal
          title={title}
          item={selectedTransaction}
          submit={submitTransaction}
          closeModal={closeModal}
          isEditItem={isEditTransaction}
          columnsToDelete={columnsToDelete}
        />
      )}
      <EmptyCard>
        <div className="unpadded-top">
          <div className="transactions-header">
            <h3>{ title }</h3>
            <div className="add-container">
              <AddBoxOutlined
                className="add-button"
                style={{ fontSize: "32px" }}
                onClick={() => addTransaction()}
              />
            </div>
          </div>
          <ListItems
            objectsArray={transactions}
            showHeader={true}
            actions={transactionsActions}
            columnsToDelete={columnsToDelete}
          />
        </div>
      </EmptyCard>
    </Wrapper>
  );
};

export default Transactions;
