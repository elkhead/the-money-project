import actionTypes from "./ActionTypes";

const setTransactions = (newVal) => ({
  type: actionTypes.SET_TRANSACTIONS,
  value: newVal,
});

const setInvestments = (newVal) => ({
  type: actionTypes.SET_INVESTMENTS,
  value: newVal,
});

const setCardBalance = (newVal) => ({
  type: actionTypes.SET_CARD_BALANCE,
  value: newVal,
});

const setCreditBalance = (newVal) => ({
  type: actionTypes.SET_CREDIT_BALANCE,
  value: newVal,
});

const setInvestmentsTotal = (newVal) => ({
  type: actionTypes.SET_INVESTMENTS_TOTAL,
  value: newVal,
});

const setTransactionsTotal = (newVal) => ({
  type: actionTypes.SET_TRANSACTIONS_TOTAL,
  value: newVal,
});

const setTotalBalance = (newVal) => ({
  type: actionTypes.SET_TOTAL_BALANCE,
  value: newVal,
});

export const actions = {
  setTransactions,
  setInvestments,
  setCardBalance,
  setCreditBalance,
  setInvestmentsTotal,
  setTransactionsTotal,
  setTotalBalance,
};
