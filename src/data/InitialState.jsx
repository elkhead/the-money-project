const initialTransactions = [
    { id: 7, date: "2021-03-05", company: "Starbucks Coffee", category: "Food", amount: 12.45},
    { id: 6, date: "2021-04-06", company: "Taco Bell", category: "Food", amount: 7.45},
    { id: 5, date: "2021-05-07", company: "H&M", category: "Clothing", amount: 75.58},
    { id: 4, date: "2021-06-08", company: "Gucci", category: "Clothing", amount: 1200.12},
    { id: 3, date: "2021-07-08", company: "Spotify", category: "Entertainment", amount: 10.12},
    { id: 2, date: "2021-08-08", company: "Sprouts", category: "Groceries", amount: 95.33},
    { id: 1, date: "2021-09-11", company: "Sprouts", category: "Groceries", amount: 95.33},
    { id: 0, date: "2021-10-14", company: "Sprouts", category: "Groceries", amount: 95.33},
];

const initialInvestments = [
    { id: 7, date: "2021-03-14", amount: 12.45},
    { id: 6, date: "2021-04-08", amount: 7.45},
    { id: 5, date: "2021-05-08", amount: 75.58},
    { id: 4, date: "2021-06-08", amount: 1200.12},
    { id: 3, date: "2021-07-05", amount: 10.12},
    { id: 2, date: "2021-08-05", amount: 95.33},
    { id: 1, date: "2021-09-05", amount: 10.12},
    { id: 0, date: "2021-10-05", amount: 95.33},
];

const initialCardBalance = 5834.99;
const initialCreditBalance = 534.99;

const initialInvestmentsTotal = Number(initialInvestments.reduce((total, investment) => investment.amount + total, 0));
const initialTransactionsTotal = Number(initialTransactions.reduce((total, transaction) => transaction.amount + total, 0));
// const initialTotalBalance = (initialInvestmentsTotal + initialCardBalance + initialCreditBalance) - initialTransactionsTotal;

export const initialState = {
    transactions: initialTransactions,
    investments: initialInvestments,
    cardBalance: initialCardBalance,
    creditBalance: initialCreditBalance,
    investmentsTotal: initialInvestmentsTotal,
    transactionsTotal: initialTransactionsTotal,
    totalBalance: 0,
}