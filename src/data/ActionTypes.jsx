const actionTypes = {
    SET_TRANSACTIONS: 'SET_TRANSACTIONS',
    SET_INVESTMENTS: 'SET_INVESTMENTS',
    SET_CARD_BALANCE: 'SET_CARD_BALANCE',
    SET_CREDIT_BALANCE: 'SET_CREDIT_BALANCE',
    SET_INVESTMENTS_TOTAL: 'SET_INVESTMENTS_TOTAL',
    SET_TRANSACTIONS_TOTAL: 'SET_TRANSACTIONS_TOTAL', 
    SET_TOTAL_BALANCE: 'SET_TOTAL_BALANCE',
}

export default actionTypes;