import { Grid } from "@mui/material";
import React from "react";
import Accounts from "./components/Accounts/Accounts";
import MainSection from "./components/MainSection/MainSection";
import SideNav from "./components/SideNav/SideNav";
import "./App.css";
import { MainProvider } from "./contexts/MainContext";

const App = () => {
  return (
    <MainProvider>
      <Grid container className="App">
        <Grid item xs={2}>
          <SideNav />
        </Grid>
        <Grid item xs={7}>
          <div className="vl" />
          <MainSection />
        </Grid>
        <Grid item xs={3}>
          <div className="vl" />
          <Accounts />
        </Grid>
      </Grid>
    </MainProvider>
  );
};

export default App;
