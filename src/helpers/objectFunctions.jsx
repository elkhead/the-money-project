export const getIsActionsEmpty = (actions) => {
  return actions === undefined || Object.entries(actions).length === 0;
};

export const getActionsCount = (actions) => {
  return Object.entries(actions).length;
};

export const getIsColumnsToDeleteEmpty = (columnsToDelete) => {
  return columnsToDelete === "" || columnsToDelete === undefined;
};

export const getColumnsToDeleteCount = (columnsToDelete) => {
  return columnsToDelete === undefined
    ? 0
    : typeof columnsToDelete === "string"
    ? columnsToDelete.length === 0
      ? 0
      : 1
    : columnsToDelete.length;
};

export const getColumnsWith = (objectsArray, actionsCount, columnsToDeleteCount) => {
  return (
    (
      100 /
      (Object.entries(objectsArray[0]).length +
        actionsCount -
        columnsToDeleteCount)
    ).toString() + "%"
  );
};

export const getHeader = (objectsArray, columnsToDelete, isColumnsToDeleteEmpty) => {
  return isColumnsToDeleteEmpty ? Object.keys(objectsArray[0]) :
  (Object.keys(objectsArray[0]).filter((value) =>
        typeof columnsToDelete === "string"
        ? value !== columnsToDelete
        : !columnsToDelete.includes(value)
    ));
};

export const isColumnVisible = (column, isColumnsToDeleteEmpty, columnsToDelete) => {
  return isColumnsToDeleteEmpty
    ? true
    : (typeof columnsToDelete === "string"
        ? column !== columnsToDelete
        : !columnsToDelete.includes(column)
    );
};

export const updateObjectInArray = (id, objectsArray, key, value) => {
  const objectIndex = objectsArray.findIndex((obj => obj.id === id));
  objectsArray[objectIndex][key] = value;
  return objectsArray;
}

export const getEmptyObjectvalues = objectToEmpty => {
  return Object.keys(objectToEmpty).reduce((obj, key) => ({ ...obj, [key]: ''}), {});
}